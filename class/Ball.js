class Ball {
  get Direction() {
    return Object.freeze({
      x: {
        RIGHT: 'RIGHT',
        LEFT: 'LEFT',
      },
      y: {
        TOP: 'TOP',
        BOTTOM: 'BOTTOM',
      },
    });
  }

  get moveSpeed() {
    return 2;
  }

  constructor() {
    this.width = 10;
    this.height = 10;
    this.x = (game.centerOfBoard.x) - (this.width / 2);
    this.y = (game.centerOfBoard.y) - (this.height / 2);
    this.directionX = this.Direction.x.RIGHT;
    this.directionY = this.Direction.y.BOTTOM;
  }

  update() {
    this.move();
    game.ctx.fillStyle = '#fff';
    game.ctx.fillRect(
      this.x,
      this.y,
      this.width,
      this.height,
    );
  }

  move() {
    this.checkCollision();
    switch (this.directionX) {
      case this.Direction.x.RIGHT: {
        this.x += this.moveSpeed;
        break;
      }
      case this.Direction.x.LEFT: {
        this.x -= this.moveSpeed;
        break;
      }
    }
    switch (this.directionY) {
      case this.Direction.y.TOP: {
        this.y -= this.moveSpeed;
        break;
      }
      case this.Direction.y.BOTTOM: {
        this.y +=this.moveSpeed;
        break;
      }
    }
  }

  checkCollision() {
    var bottomOfBall = this.y + this.height;
    var rightSideOfBall = this.x + this.width;

    if (bottomOfBall >= game.canvas.height)
      this.directionY = this.Direction.y.TOP;
    else if (this.y <= 0)
      this.directionY = this.Direction.y.BOTTOM;

    if (rightSideOfBall >= game.canvas.width || this.x <= 0)
      this.reset();

    this.checkPaddleCollision();
  }

  checkPaddleCollision() {
    var rightSideOfPlayer = player.x + player.width;
    var rightSideOfBall = this.x + this.width;
    var bottomOfPlayer = player.y + player.height;
    var bottomOfComputer = computer.y + computer.height;
    var bottomOfBall = this.y + this.height;

    if (this.directionX === this.Direction.x.LEFT) {
      var landedOnPaddle =
        Boolean(bottomOfBall > player.y && this.y < bottomOfPlayer);
      if (this.x <= rightSideOfPlayer && landedOnPaddle) {
        this.directionX = this.Direction.x.RIGHT;
        if (this.y > player.y + player.y - player.height / 2)
          this.directionY = this.Direction.y.BOTTOM;
        else
          this.directionY = this.Direction.y.TOP;
      }
    } else {
      var landedOnPaddle =
        Boolean(bottomOfBall > computer.y && this.y < bottomOfComputer);
      if (rightSideOfBall >= computer.x && landedOnPaddle) {
        this.directionX = this.Direction.x.LEFT;
        if (this.y > computer.y + computer.y - computer.height / 2)
          this.directionY = this.Direction.y.BOTTOM;
        else
          this.directionY = this.Direction.y.TOP;
      }
    }
  }

  reset() {
    this.x = (game.centerOfBoard.x) - (this.width / 2);
    this.y = (game.centerOfBoard.y) - (this.height / 2);
    this.directionX = this.directionX === this.Direction.x.RIGHT
      ? this.Direction.x.LEFT
      : this.Direction.x.RIGHT;
  }
}

