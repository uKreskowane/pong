class Player extends Paddle {
  constructor() {
    super();
    this.x = 10;
    window.addEventListener('keydown', this.keyDown.bind(this));
  }

  keyDown(e) {
    switch (e.keyCode) {
      case 40: {
        var bottomOfPaddle = this.y + this.height;
        if (bottomOfPaddle < game.canvas.height)
          this.move(1);
        break;
      }
      case 38: {
        var topOfPaddle = this.y;
        if (topOfPaddle > 0)
          this.move(-1);
        break;
      }
    }
  }
}

