class Paddle {
  get moveSpeed() {
    return 2;
  }

  constructor() {
    this.width = 10;
    this.height = 100;
    this.x = game.canvas.width - (10 + this.width);
    this.y = (game.canvas.height / 2) - (this.height / 2);
  }
  
  update() {
    game.ctx.fillStyle = '#fff';
    game.ctx.fillRect(
      this.x,
      this.y,
      this.width,
      this.height,
    );
  }
  
  move(y) {
    this.y += y * this.moveSpeed;
  }
}

