class Game {
  constructor(selector) {
    this.canvas = document.querySelector(selector);
    this.ctx = this.canvas.getContext('2d');
    this.drawBoard()
  }

  get centerOfBoard() {
    return {
      x: this.canvas.width / 2,
      y: this.canvas.height / 2,
    };
  }
  
  drawBoard() {
    this.ctx.fillStyle = '#000';
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }
}
