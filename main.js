var game = new Game('#game');
var player = new Player();
var computer = new Paddle();
var ball = new Ball();

// Game loop
setInterval(function () {
  game.drawBoard();
  player.update();
  computer.update();
  ball.update();
}, 1000 / 60);

